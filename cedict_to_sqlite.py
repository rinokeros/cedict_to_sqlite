#!/usr/bin/env python3
import sqlite3
import gzip
from pathlib import Path
from argparse import ArgumentParser
import requests
from pinyin import convert_pinyin
from pinyin_no_tone import convert_pinyin_no_tone


class CLI:
    """ Very basic command line interface to convert a cedict file to a sqlite
        database. """

    WEB_CEDICT_FILE = ("https://www.mdbg.net/chinese/export/cedict/"
                       "cedict_1_0_ts_utf-8_mdbg.txt.gz")

    def __init__(self):
        Path("build/").mkdir(exist_ok=True)

        self.init_args()
        self.download_cedict()
        self.init_db()
        self.populate_db()

    def init_args(self):
        """ Inits the argument parser. """

        parser = ArgumentParser(
            description="Converts cedict to a sqlite database.")
        parser.add_argument("--enable-tone-accents",
                            dest="enable_tone_accents",
                            default=False, type=bool,
                            help="Boolean toggle to add pinyin with character "
                            "tones as seperate column. Defaults to False.")
        parser.add_argument("--disable-tone",
                            dest="disable_tone",
                            default=True, type=bool,
                            help="Boolean toggle to remove pinyin tone "
                            "remove tone number as seperate column. Defaults to True.")
        parser.add_argument("--erhua-keep-space",
                            dest="erhua_keep_space",
                            default=False, type=bool,
                            help="Boolean toggle to keep space before r if "
                            "--enable-tone-accents is set to true. "
                            "Defaults to False.")
        self.args = parser.parse_args()

    def download_cedict(self):
        """ Downloads the cedict file and stores it on the filesystem. """

        if not Path("build/cedict.txt.gz").is_file():
            with open("build/cedict.txt.gz", "wb") as file:
                file.write(requests.get(self.WEB_CEDICT_FILE).content)

    def init_db(self):
        """ Drops the cedict database if it already exists, and then creates
            the database. """

        self.conn = sqlite3.connect("build/cedict.db")
        cursor = self.conn.cursor()
        cursor.execute("DROP TABLE IF EXISTS entries")
        cursor.execute("CREATE TABLE entries (traditional TEXT,"
                       "simplified TEXT, short TEXT)")

        if self.args.enable_tone_accents:
            cursor.execute("ALTER TABLE entries "
                           "ADD COLUMN pinyin_char_tone TEXT")

        if self.args.disable_tone:
            cursor.execute("ALTER TABLE entries "
                           "ADD COLUMN pinyin_no_tone TEXT")

        cursor.execute("CREATE INDEX entries_index "
                       "ON entries (traditional, simplified)")
        cursor.close()

    def populate_db(self):
        """ Parses the cedict text file, and populates the cedict database
            with the relevant fields. """

        cursor = self.conn.cursor()

        with gzip.open("build/cedict.txt.gz", "rt", encoding="utf-8") as file:
            for line in file:
                if line[0] == "#":
                    continue

                trad, simp = line.split(" ")[:2]
                pinyin = line[line.index("[") + 1:line.index("]")]
                line = pinyin
                words = line.split()
                letters = [word[0] for word in words]
                letters = "".join(letters)
                pinyin = pinyin.replace("r5", "r")
                pinyin = pinyin.replace(" ", "")

                if self.args.enable_tone_accents:
                    pinyin_char_tone = convert_pinyin(pinyin)
                    if self.args.erhua_keep_space:
                        pinyin_char_tone = pinyin_char_tone.replace("r5", "r")
                    else:
                        pinyin_char_tone = pinyin_char_tone.replace(" r5", "r")
                    # Some of the pinyin is capitalized so that's why I'm
                    # leaving the preceding l out.
                    pinyin_char_tone = pinyin_char_tone.replace("u:2", "ǘ")
                    pinyin_char_tone = pinyin_char_tone.replace("u:3", "ü")
                    pinyin_char_tone = pinyin_char_tone.replace("u:4", "ǜ")

                    cursor.execute("INSERT INTO entries (traditional,"
                                   "simplified,"
                                   "pinyin_char_tone) VALUES (?,?,?)",
                                   (trad, simp,
                                    pinyin_char_tone))

                if self.args.disable_tone:
                    pinyin_no_tone = convert_pinyin_no_tone(pinyin)
                    if self.args.erhua_keep_space:
                        pinyin_no_tone = pinyin_no_tone.replace("r5", "r")
                    else:
                        cursor.execute("INSERT INTO entries (traditional,"
                                   "simplified, short,"
                                   "pinyin_no_tone) VALUES (?,?,?,?)",
                                   (trad, simp, letters,
                                    pinyin_no_tone))

                else:
                    cursor.execute("INSERT INTO entries (traditional,"
                                   "simplified) "
                                   "VALUES (?,?)",
                                   (trad, simp))

        cursor.close()
        self.conn.commit()


CLI()
