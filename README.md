# cedict\_to\_sqlite
Converts CEDICT into a SQLite database.

# Requirements
* Python 3+ 

Debian: `apt install python3-pip` and `pip3 install pipenv`

# Setup
`pipenv install`

# Usage
`pipenv run python3 cedict_to_sqlite.py`
