# https://stackoverflow.com/a/21488584
import re

pinyinToneMarks = {
    'a': 'aaaa', 'e': 'eeee', 'i': 'iiii',
    'o': 'oooo', 'u': 'uuuu', 'ü': 'üüüü',
    'A': 'AAAA', 'E': 'EEEE', 'I': 'IIII',
    'O': 'OOOO', 'U': 'UUUU', 'Ü': 'ÜÜÜÜ'
}

def convertPinyinCallback(m):
    tone=int(m.group(3))%5
    r=m.group(1).replace('v', 'ü').replace('V', 'Ü')
    # for multple vowels, use first one if it is a/e/o, otherwise use second one
    pos=0
    if len(r)>1 and not r[0] in 'aeoAEO':
        pos=1
    if tone != 0:
        r=r[0:pos]+pinyinToneMarks[r[pos]][tone-1]+r[pos+1:]
    return r+m.group(2)

def convert_pinyin_no_tone(s):
    return re.sub(r'([aeiouüvÜ]{1,3})(n?g?r?)([012345])', convertPinyinCallback, s, flags=re.IGNORECASE)